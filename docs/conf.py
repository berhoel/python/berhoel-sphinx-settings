"""Configuration file for the Sphinx documentation builder."""

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
from __future__ import annotations

copyright = "2022, 2024 Berthold Höllmann <berhoel@gmail.com>"  # noqa:A001

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


# -- theme selection -------------------------------------------------

from berhoel.sphinx_settings import *  # noqa: E402,F403, isort:skip
from berhoel.sphinx_settings import (  # noqa:E402, isort:skip
    configuration,
    get_html_theme_path,
)

globals().update(configuration().configuration())

html_theme = "berhoel_sphinx_theme"
html_theme_path = get_html_theme_path()

html_static_path = ["_static"]

html_theme_options = {
    "extra_nav_links": {
        "GitLab": "https://gitlab.com/berhoel/python/berhoel-sphinx-settings",
    },
}
