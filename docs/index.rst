:gitlab_url:
   "https://gitlab.com/berhoel/python/berhoel-sphinx-settings"

.. include:: ../README.rst

API Documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   sphinx_settings

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   _tmp/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
