"""Configurations for tests."""

from __future__ import annotations

from contextlib import contextmanager
import os
from pathlib import Path
from typing import TYPE_CHECKING

import pytest

if TYPE_CHECKING:
    from collections.abc import Generator


@pytest.fixture
def set_directory():
    @contextmanager
    def _set_directory(path: Path) -> Generator:
        """Set the cwd within the context.

        Args:
            path: The path to the cwd

        Yields
        ------
            None

        """
        origin = Path().absolute()
        try:
            os.chdir(path)
            yield
        finally:
            os.chdir(origin)

    return _set_directory
