"""Testing parsing of pyproject.toml files."""

from __future__ import annotations

from typing import TYPE_CHECKING

import pytest

from berhoel.sphinx_settings import PyProject_toml, configuration

if TYPE_CHECKING:
    from pathlib import Path


@pytest.fixture
def sample(tmp_path: Path) -> Path:
    """Prepare sample `pyproject.toml` file.

    Returns
    -------
        points to geneated sample file

    """
    res = tmp_path / "pyproject.toml"
    res.write_text(
        """[project]
name = "test_project"
version = "0.0.1"
description = "Sphinx settings for documenting my projects."
packages = [{ include = "berhoel" }]
exclude = ["**/.#*.py", "**/#*.py#"]
authors = [{name = "Berthold Höllmann", email = "berthold@xn--hllmanns-n4a.de"}]

[tool.poetry]
""",
    )
    return res


@pytest.fixture
def py_project_toml(sample) -> PyProject_toml:
    return PyProject_toml(sample)


def test_project(py_project_toml) -> None:
    assert py_project_toml.project == "test_project"


def test_release(py_project_toml) -> None:
    assert py_project_toml.release == "0.0.1"


def test_author(py_project_toml) -> None:
    assert py_project_toml.author == "Berthold Höllmann <berthold@xn--hllmanns-n4a.de>"


def test_configuration() -> None:
    probe = configuration()
    assert probe.project == "berhoel-sphinx-settings"
    assert probe.release
