"""Testing parsing of pyproject.toml files."""

from __future__ import annotations

from typing import TYPE_CHECKING

import pytest

from berhoel.sphinx_settings import CMakeLists_txt, ProjectTypes, configuration

if TYPE_CHECKING:
    from pathlib import Path


@pytest.fixture
def sample(tmp_path) -> Path:
    res = tmp_path / "CMakeLists.txt"
    res.write_text(
        """cmake_minimum_required(version 3.16)

project(test_project VERSION 0.0.1 LANGUAGES CXX)
""",
    )
    return res


@pytest.fixture
def py_project_toml(sample) -> CMakeLists_txt:
    return CMakeLists_txt(sample)


def test_project(py_project_toml) -> None:
    assert py_project_toml.project == "test_project"


def test_release(py_project_toml) -> None:
    assert py_project_toml.release == "0.0.1"


def test_author(py_project_toml) -> None:
    assert py_project_toml.author == "Berthold Höllmann <berthold@xn--hllmanns-n4a.de>"


def test_configuration(sample, set_directory) -> None:
    with set_directory(sample.parent):
        probe = configuration(ProjectTypes.CMAKE)
        assert probe.project == "test_project"
        assert probe.release == "0.0.1"
        assert probe.author == "Berthold Höllmann <berthold@xn--hllmanns-n4a.de>"


def test_configuration_2(sample, set_directory) -> None:
    subdir = sample.parent / "deeper" / "down"
    subdir.mkdir(parents=True)
    sub_cmake = subdir / "CMakeLists.txt"
    sub_cmake.write_text("# Only a meaningless sample")
    with set_directory(subdir):
        probe = configuration(ProjectTypes.CMAKE)
        assert probe.project == "test_project"
        assert probe.release == "0.0.1"
        assert probe.author == "Berthold Höllmann <berthold@xn--hllmanns-n4a.de>"
