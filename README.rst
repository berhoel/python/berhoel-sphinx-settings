=========================
 berhoel-sphinx-settings
=========================

Sphinx theme for documenting my projects
========================================

Sphinx settings for documenting my projects

This theme sets also some project information, such as release (read from
the ``pyproject.toml`` file) and the author.
