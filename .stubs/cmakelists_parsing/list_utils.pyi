def merge_pairs(
    list: object,  # noqa:A002
    should_merge: object,
    merge: object,
) -> object: ...
